package br.ufv.caf.engenhariadesoftware2.testemutacao;

import java.util.Arrays;
import java.util.Random;

import junit.framework.TestCase;

public class MergeSortTests extends TestCase {
	int arr[];
	Random rand;
	protected void setUp() {
		rand = new Random();
		arr = new int[30];
		for(int i = 0; i < arr.length; i++) {
			arr[i] = rand.nextInt();
		}
	}
	public void testMerge() {
		int expected[] = Arrays.copyOf(arr, arr.length);
		Arrays.sort(expected);
		MergeSort.mergesort(arr);
		for(int i = 0; i < arr.length; i++) {
			assertEquals(expected[i], arr[i]);
		}
		
	}
}
